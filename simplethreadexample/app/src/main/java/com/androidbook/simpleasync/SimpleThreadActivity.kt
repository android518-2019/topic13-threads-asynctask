package com.androidbook.simpleasync

/**
 * This class is the example from the book chapter you have on moodle
 * From Android Wireless Application Development Volume II Chapter 1
 *
 * Uses Java Threads
 * Meant to contrast with AsyncTask do not implement in your code.
 * Must be managed by programmer.
 *
 */
import android.app.Activity
import android.os.Bundle
import android.os.SystemClock
import android.widget.TextView
import android.widget.Toast

class SimpleThreadActivity : Activity() {
    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.count)

        val tv = findViewById(R.id.counter) as TextView
        val msg = findViewById(R.id.text) as TextView
        msg.setText(R.string.javathread)
        Toast.makeText(this, "java thread", Toast.LENGTH_LONG).show()
        // instantiate the Thread with a Runnable()
        Thread(Runnable // start the thread
        {
            var i = 0

            while (i < 100) {
                SystemClock.sleep(500)
                i++

                val curCount = i
                if (curCount % 5 == 0) {
                    /*
                    	 * View.post(Runnable) causes
                    	 * the Runnable to be added to the message queue.
                    	 * The runnable will be run on the user interface thread
                    	 */
                    tv.post {
                        // update UI with progress every 5%
                        tv.text = "$curCount% Complete!"
                    }
                }
            }

            tv.post { tv.text = "Count Complete!" }
        }).start()

    }

}