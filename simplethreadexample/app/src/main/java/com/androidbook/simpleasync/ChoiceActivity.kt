package com.androidbook.simpleasync

/**
 * This class is the example from the book chapter you have on moodle
 * From Android Wireless Application Development Volume II Chapter 1
 */
import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View

class ChoiceActivity : Activity() {
    /** Called when the activity is first created.  */
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.main)
    }

    fun buttonOnClickThread(v: View) {
        startActivity(Intent(this, SimpleThreadActivity::class.java))
    }

    fun buttonOnClickAsync(v: View) {
        startActivity(Intent(this, SimpleAsyncActivity::class.java))
    }

    fun buttonOnClickInUIThread(v: View) {
        startActivity(Intent(this, SimpleNoBGThreadActivity::class.java))
    }

}