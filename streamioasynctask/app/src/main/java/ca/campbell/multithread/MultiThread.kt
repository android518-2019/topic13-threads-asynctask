package ca.campbell.multithread

import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.FileNotFoundException
import java.io.IOException
import java.io.InputStreamReader

import android.app.Activity
import android.os.AsyncTask
import android.os.Bundle
import android.os.Environment
import android.os.Environment.getExternalStoragePublicDirectory
import android.text.format.Time
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.View.OnClickListener
import android.widget.Button
import android.widget.TextView
import org.w3c.dom.Text

/**
 * This code is an example of doing I/O in a background thread
 * using AsyncTask.  This example is atypical as it only uses the
 * background thread by overriding doInBackground()
 *
 * remember AsyncTask is good for fairly short running tasks
 * anything long running Java Threads or Kotlin concurrency are better
 *
 *  The I/O done here is to read a file from the
 *  Android Downloads directory path
 *
 *  To run this code:
 *
 *  Download ascii.txt from http://bit.ly/518droid
 *
 *  watch the log adb logcat -s MTHREAD
 *  note that the I/O will probably finish before you can page through any of it
 *
 *  you can create a large file and download it as ascii.txt
 *  watch the log adb logcat -s MTHREAD
 *  note that the I/O will probably continue in the background
 *  as you are paging through the file *
 *
 *  @author PMCampbell
 *  @version today
 */
class MultiThread : Activity() {
    private var aboutTVoff = true
    lateinit var tv: TextView
    lateinit var tv2: TextView
    private var data = ""
    private var today: Time? = null
    lateinit var FN: File
    private val TAG = "MTHREAD"
    internal var start = 0
    internal var gdAsync: GetDataAsync? = null

    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_multi_thread)

        tv2 = findViewById(R.id.tv2) as TextView
        tv = findViewById(R.id.tv1) as TextView
        today = Time(Time.getCurrentTimezone())
        today!!.setToNow()
        FN = File(getExternalFilesDir(Environment.DIRECTORY_DOWNLOADS), "ascii.txt")
        if (!FN.exists()) {
            (findViewById<TextView>(R.id.tv3)).visibility = View.VISIBLE
            (findViewById<TextView>(R.id.tv3)).text = getString(R.string.filedoesnotexist)
            Log.e(TAG, FN.name)
            Log.e(TAG, FN.canonicalPath)
            Log.e(TAG, FN.absolutePath)
            Log.e(TAG, FN.path)

        }
    }
    /*
	 * (non-Javadoc)
	 * @see android.view.View.OnClickListener#onClick(android.view.View)
	 */
    /**
     *  load file
     */
    fun loadFileBT(v: View) {
        Log.d(TAG, "loadFileBT data leng " + data.length + " start " + start)

            // if thread still running cancel before new thread
            // I only want 1 background thread
            if (gdAsync != null)
                gdAsync!!.cancel(true)

            gdAsync = GetDataAsync()
            gdAsync!!.execute(FN)
            start = 0   // start at beginning of buffer
            if (data.isEmpty()) {
                tv.text = "finished " + today!!.format("%F: %T") + "\nno data"
            } else {
                val str = ("finished " + today!!.format("%F: %T") + "\n "
                        + data.substring(start, if (data.length >= 100) 100 else data.length))
                tv.text = str
                (findViewById(R.id.button2) as Button).visibility = View.VISIBLE
            }
        }

    /**
     * load next 100 bytes
     */
    fun next100BT(v: View) {
        Log.d(TAG, "next100BT data leng " + data.length + " start " + start)

        if (data.length > start + 100) {
                start += 100
                val remaining = data.length - start
                tv!!.text = ("finished " + today!!.format("%F: %T") + "\n "
                        + data.substring(start, if (remaining >= 100) start + 100 else start + remaining))
            } else {
                tv!!.text = "End of Data"
            }

        }

    /*
	 * Simple form of AsyncTask We do only the background task, none of the
	 * callback methods that run on the UI are used.
	 *
	 * The only parameter is the file name.
	 *
	 * progress and result are not used hence Void
	 */
    inner class GetDataAsync : AsyncTask<File, Void, Void>() {
        // runs on background thread
        override fun doInBackground(vararg fn: File): Void? {
            Log.d(TAG, "Reading: " + fn[0].absoluteFile)

            getData(fn[0])
            return null
        }

        // runs on main ui thread
        override fun onCancelled() {
            Log.d(TAG, "BG Thread Canceled")
        }
    } // AsyncTask class

    /**
     * getData() This method opens a FileInputStream, then uses a BufferedReader
     * to read the data in line by line catches file not found & loading
     * exceptions
     */
    private fun getData(fn: File) {
        var line: String? = null
        data = ""
        try {
            Log.d(TAG, "getData() ${fn.name}")
            val fis = FileInputStream(fn)
            val reader = BufferedReader(InputStreamReader(fis))
            // check if canceled:
            // getData is run from an AsyncTask
            // which can be canceled by outside forces
            // while (!gdAsync!!.isCancelled && ((line = reader.readLine() != null)) {
            while (!gdAsync!!.isCancelled ) {
                data += line!! + "\n"
                Log.d(TAG, "line:  $line")
            }
            reader.close()
            fis.close()
            Log.d(TAG, "File successfully loaded.")
        } catch (e: FileNotFoundException) {
            Log.d(TAG, "Error file not found: " + e.localizedMessage)
            data = "Error file not found: " + e.localizedMessage
        } catch (e: IOException) {
            Log.d(TAG, "Error loading file: " + e.localizedMessage)
            data = "Error loading file: " + e.localizedMessage
            e.printStackTrace()
        }

    } // getData()

    /*
	 * User presses back key, abort the background task if running
	 *
	 * (non-Javadoc)
	 * @see android.app.Activity#onBackPressed()
	 */
    override fun onBackPressed() {
        super.onBackPressed()
        if (gdAsync != null)
            gdAsync!!.cancel(true)
    }

    /*
	 * If we're ending the task , abort the background task if running,
	 * and not already canceled.
	 *
	 * (non-Javadoc)
	 * @see android.app.Activity#onStop()
	 */
    public override fun onStop() {
        super.onStop()
        if (gdAsync != null && !gdAsync!!.isCancelled) {
            gdAsync!!.cancel(true)
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu; this adds items to the action bar if it is present.
        menuInflater.inflate(R.menu.activity_single_thread, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.about -> {
                if (aboutTVoff) {
                    tv2.visibility = View.VISIBLE
                    aboutTVoff = false
                } else {
                    tv2.visibility = View.GONE
                    aboutTVoff = true
                }
                return true
            }
            else -> return false
        }
    }
} // Activity class
